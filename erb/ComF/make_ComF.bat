@echo off
echo 생성한 후 인코딩을 UTF-8로 다시 저장해 주어야함..
SETLOCAL ENABLEDELAYEDEXPANSION
for /l %%i in (105,1,105) do (
	echo ;ComF%%i > ComF%%i.erb
	echo ; 커맨드 이름: >> ComF%%i.erb
	echo ; 파생가능 커맨드 목록: >> ComF%%i.erb
	echo ;      파생: 파생커맨드이름^(번호^): 조건1, 조건2, 조건3 >> ComF%%i.erb
	echo ;      파생: 파생커맨드이름^(번호^): 조건1, 조건2, 조건3 >> ComF%%i.erb
	echo ; 트리거 커맨드: >> ComF%%i.erb
	echo. >> ComF%%i.erb

	echo ;- COM_ABLE -------------------------------------- >> ComF%%i.erb
	echo ;실행 가능성 판정하는 시스템 함수. 커스텀커맨드를 사용하기 때문에 무조건 0리턴해야함 >> ComF%%i.erb
	echo @COM_ABLE%%i >> ComF%%i.erb
	echo RETURN 0 >> ComF%%i.erb
	echo. >> ComF%%i.erb

	echo ;실제로 사용하는 가능성 판정^(파생조교는 기본적으로 트리거조교의 조건을 만족한다^) >> ComF%%i.erb
	echo @EX_COM_ABLE%%i >> ComF%%i.erb
	echo ;판정시에는 PLAYER 대신 ATTACKER 사용 >> ComF%%i.erb
	echo ;판정시에는 TARGET 대신 DEFENDER 사용 >> ComF%%i.erb
	echo. >> ComF%%i.erb	
	echo RETURN 1 >> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	
	echo. >> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	echo ;출력하는 커맨드 이름 ^(기본값은 TRAINNAME^) >> ComF%%i.erb
	echo ;주도권반전 중에 이름을 변경하거나, 지속성 명령을 중료할때 이름 변경한다.>> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	echo @COM_NAME%%i >> ComF%%i.erb
	echo IF TFLAG:주도권반전 >> ComF%%i.erb
	echo 	TSTR:조교커맨드이름 = 출력이름 >> ComF%%i.erb
	echo ELSE >> ComF%%i.erb
	echo 	TSTR:조교커맨드이름 = %%TRAINNAME:%%i%% >> ComF%%i.erb
	echo ENDIF >> ComF%%i.erb

	echo. >> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	echo ;파생 조교가 존재하는지 판정하는 용도 >> ComF%%i.erb
	echo ;출력에서 보여주지 않으려면 0을 돌려주고, COM에서 JUMP 처리해도됨 >> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	echo ;@EXIST_DERIVED_TRAIN%%i >> ComF%%i.erb
	echo ;CALL EX_COM_ABLExxx >> ComF%%i.erb
	echo ;SIF RESULT > 0 >> ComF%%i.erb
	echo ;	RETURN 번호 >> ComF%%i.erb
	echo ;RETURN 0 >> ComF%%i.erb

	echo. >> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	echo ;실제 실행하는 시스템 함수 >> ComF%%i.erb
	echo ;실행판정→주도반전→장비탈부착→조교시작메시지→소스추가→더러움추가→경험추가 순 >> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	echo @COM%%i >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo PLAYER_BACKUP = PLAYER >> ComF%%i.erb
	echo TARGET_BACKUP = TARGET >> ComF%%i.erb
	echo ;실제 실행 가능성 판정, 필요시 적용한다. ^(키스, 자위 등..^) >> ComF%%i.erb
	echo ;CALL COM_ORDER >> ComF%%i.erb
	echo ;SIF RESULT == 0 >> ComF%%i.erb
	echo ;	RETURN 0 >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo PRINTFORML %%TSTR:조교커맨드이름%% >> ComF%%i.erb
	echo SETBIT TFLAG:커맨드속성판정, 커맨드_속성값 >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo IF TFLAG:주도권반전 >> ComF%%i.erb
	echo 	TARGET = PLAYER_BACKUP >> ComF%%i.erb
	echo 	PLAYER = TARGET_BACKUP >> ComF%%i.erb
	echo ENDIF >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo ; 장착 및 장착 해제 처리 Message쪽에서 장착해제도 처리한다. 동일 명령 실행 시, 예외처리 추가해야함 >> ComF%%i.erb
	echo ; CALL TRAIN_MESSAGE_UNEQUIP^(TARGET, GETNUM^(TEQUIP, "장착부위1"^)^) >> ComF%%i.erb
	echo ; CALL TRAIN_MESSAGE_UNEQUIP^(PLAYER, GETNUM^(TEQUIP, "장착부위2"^)^) >> ComF%%i.erb
	echo ; TEQUIP:장착부위1 = %%i >> ComF%%i.erb
	echo ; TEQUIP:장착부위1대상 = PLAYER >> ComF%%i.erb
	echo ; TEQUIP:PLAYER:장착부위2 = %%i >> ComF%%i.erb
	echo ; TEQUIP:PLAYER:장착부위2대상 = TARGET >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo ;명령 이벤트 대사 출력^(전^) >> ComF%%i.erb
	echo CALL TRAIN_MESSAGE_BEFORE >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo ; 체력,기력 소모 >> ComF%%i.erb
	echo DOWNBASE:PLAYER:체력 = 체력소모값>> ComF%%i.erb
	echo DOWNBASE:PLAYER:기력 = 기력소모값>> ComF%%i.erb
	echo DOWNBASE:TARGET:체력 = 체력소모값>> ComF%%i.erb
	echo DOWNBASE:TARGET:기력 = 기력소모값>> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo ; 소스 추가 >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo ; 더러움 처리 >> ComF%%i.erb
	echo STAIN:타겟 ^|= STAIN:소스 >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo ; 경험 처리 >> ComF%%i.erb
	echo TCVAR:경험 = 경험값 >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo IF TFLAG:주도권반전 >> ComF%%i.erb
	echo 	TARGET = TARGET_BACKUP >> ComF%%i.erb
	echo 	PLAYER = PLAYER_BACKUP >> ComF%%i.erb
	echo ENDIF >> ComF%%i.erb
	echo. >> ComF%%i.erb
	echo RETURN 1 >> ComF%%i.erb

	echo. >> ComF%%i.erb
	echo ;- EQUIPMENT ------------------------------------- >> ComF%%i.erb
	echo ;장착 중 추가 처리 >> ComF%%i.erb
	echo ;장착 메시지 추가 시 ^(TRAIN_MESSAGE_EQUIP에 추가하자^) >> ComF%%i.erb
	echo ;@EQUIP_COM%%i >> ComF%%i.erb
	echo ;CALL TRAIN_MESSAGE_EQUIP >> ComF%%i.erb
	echo ;장착중 체력, 기력 소모 >> ComF%%i.erb
	echo ;장착중 소스 추가 >> ComF%%i.erb
	echo ;장착중 경험 추가 >> ComF%%i.erb
	
	echo. >> ComF%%i.erb
	echo ;이미지 출력용 장착부위 및 이미지 리소스 이름 저장 >> ComF%%i.erb
	echo ;@GET_EQUIP%%i >> ComF%%i.erb
	echo ;TSTR:X장착이름 = X_아이템이름 >> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	
	echo. >> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	echo ;실제 실행 가능성 판정 >> ComF%%i.erb
	echo ;거절 메시지 추가 시 ^(TRAIN_MESSAGE_DENIAL에 추가하자^) >> ComF%%i.erb
	echo ;------------------------------------------------- >> ComF%%i.erb
	echo ;@COM_ORDER_%%i >> ComF%%i.erb
	echo ;TFLAG:판정값 >> ComF%%i.erb
)

pause